> ### THIS REPO AND ITS CONTRACTS HAVE **NOT** BEEN AUDITED BY A THIRD-PARTY, see license.txt for usage details

# @zCircuit/Timelock

> Guaranteed lock of funds for a user specified time

This project's responsibility is to hold any asset or algos for a predetermined length of time.  A single account can have up to 16 escrows simultaneously.  Each escrow is identified by a user defined key.  Every escrow will operate independently, each can hold different assets and lock funds for different lengths of time.  At the time of creating an escrow, the unlock time is specified.  This unlock time can be extended but it can NEVER be shortened. For now, only the escrow creator can withdraw funds when the time limit is exceeded.

Possible use-cases:
 - Creating staking contracts that predicate user funds are inaccessible for a length of time
 - Implement the 'cliff' of a vesting schedule

## Timelock User Lifecycle

 1. **Opt-in to the escrow**. The escrow will need to maintain some user specific state, requiring an opt-in to maintain the information on-chain.
 1. **Create and deposit algos/assets into escrow**.  A user will create a new escrow, potentially with an application specific key, depositing their funds for a length of time.
    1. **(Optional) Extend the escrow time**. Depending on developer use-case it may be convenient to have users extend an escrow instead of withdrawing and redepositing.
 1. **Withdraw escrowed funds**. When the time finally comes, the user can withdraw 100% of their locked funds.

## How to interact with an existing faucet

Add the `@zcircuit/timelock` package to your projects dependencies.  This package is not published to npmjs.org so you will need to configure your `@zcircuit` npm registry.

1. Create a `.npmrc` file in your project at the same level as your `package.json` (If you don't already have one). Add the following to your `.npmrc`
    ```
    @zcircuit:registry=https://gitlab.com/api/v4/projects/35806759/packages/npm/
    ```

1. Add the `@zcircuit/timelock` package to your project
    ```bash
    npm install @zcircuit/timelock
    ```

1. Construct an instance of the SDK, providing an algod client, indexer client, and a strategy for signing transactions
    ```ts
    import { TimelockSdk } from '@zcircuit/timelock';

    const timelockSdk = new TimelockSdk(
        algod,
        indexer,
        signer,
    );
    ```

> For more advanced use-cases where you need to contruct your own transaction groups, use the TimelockTransactions class
```ts
import { TimelockTransactions } from '@zcircuit/timelock';

const timelockTxns = new TimelockTransactions();
```

**For more examples using the TimelockSdk refer to `./algorand/test` where the sdk is used extensively to validate expected behaviours***

## How to deploy a Timelock contract to Algorand

> Again, these contracts have NOT been security audited, refer to license.txt for usage details
>   - We do ask that if you perform an audit of this code for your own purposes, please share the results publicly and we can include the audit details with the repo

1. Follow the steps above to configure the `@zcircuit` npm registry
1. Add the `@zcircuit/timelock-deployer` package to your project
    ```bash
    npm install @zcircuit/timelock-deployer
    ```
1. Use the exported `deployTimelock` method
   - This package also includes a `getDeployTimelockTransaction` to get just the deploy transaction, as well as a couple other helpful functions

**Deploy Configuration**
This contract does not require any configuration when deploying

## Repo Introduction

This repo contains the source for 2 modules: `@zcircuit/timelock` & `@zcircuit/timelock-deployer`. Both packages provide typescript type declarations. The repo is organized into 3 source folders:

 - `./algorand`: The source for the Algorand pyTeal smart contract implementations.  Also included is an extensive test suite that demonstrates and ensures expected contract behaviour. These tests make use of the Timelock SDK which also demonstrates how to use the SDK effectively.
 - `./deployer`: The source for `@zcircuit/timelock-deployer`. This module can be used to deploy your own instance of a Timelock escrow to the Algorand blockchain without requiring the pyTeal environment setup.
 - `./sdk`: The source for `@zcircuit/timelock`. This module is the Timelock SDK which can be used to interact with any instance of a deployed Timelock.
