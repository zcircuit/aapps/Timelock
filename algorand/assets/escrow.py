from pyteal import *
from parse_params import parse_params
import sys
sys.path.append('..')

compileParams = {}


def optInToAsset(xferAsset):
    optInPaymentIndex = Txn.group_index() - Int(1)

    return Seq([
        Assert(And(
            Gtxn[optInPaymentIndex].receiver() ==
            Global.current_application_address(),
            Gtxn[optInPaymentIndex].amount() == Int(100000),
        )),
        InnerTxnBuilder.Begin(),
        InnerTxnBuilder.SetField(
            TxnField.type_enum, TxnType.AssetTransfer),
        InnerTxnBuilder.SetField(TxnField.xfer_asset, xferAsset),
        InnerTxnBuilder.SetField(TxnField.asset_amount, Int(0)),
        InnerTxnBuilder.SetField(
            TxnField.asset_receiver, Global.current_application_address()),
        InnerTxnBuilder.SetField(TxnField.fee, Int(0)),
        InnerTxnBuilder.Submit(),
    ])


def createLock():
    lockId = Txn.application_args[1]
    unlockDate = Txn.application_args[2]
    existingLock = App.localGetEx(Txn.sender(), Txn.application_id(), lockId)
    lockDate = Itob(Global.latest_timestamp())

    paymentIndex = Txn.group_index() + Int(1)
    payTxn = Gtxn[paymentIndex]
    xferAsset = payTxn.xfer_asset()
    escrowAssetHolding = AssetHolding.balance(
        Global.current_application_address(), xferAsset)

    return Seq([
        existingLock,
        Assert(Not(existingLock.hasValue())),
        Cond(
            [Gtxn[paymentIndex].type_enum() == TxnType.Payment, Seq([
                Assert(Gtxn[paymentIndex].receiver() ==
                       Global.current_application_address()),
                App.localPut(Txn.sender(), lockId, Concat(
                    unlockDate, lockDate,
                    Itob(Int(0)),
                    Itob(payTxn.amount()),
                )),
            ])],
            [Gtxn[paymentIndex].type_enum() == TxnType.AssetTransfer, Seq([
                escrowAssetHolding,
                If(Not(escrowAssetHolding.hasValue())).Then(
                    optInToAsset(xferAsset)),
                App.localPut(Txn.sender(), lockId, Concat(
                    unlockDate, lockDate,
                    Itob(xferAsset),
                    Itob(payTxn.asset_amount()),
                )),

            ])],
        ),
        Approve(),
    ])


def addToLock():
    return Seq([Approve()])


def exitLock():
    lockId = Txn.application_args[1]
    lockStateMaybe = App.localGetEx(Txn.sender(), Txn.application_id(), lockId)
    lockEndDate = Btoi(Extract(lockStateMaybe.value(), Int(0), Int(8)))
    lockStartDate = Btoi(Extract(lockStateMaybe.value(), Int(8), Int(8)))
    lockAssetId = Btoi(Extract(lockStateMaybe.value(), Int(16), Int(8)))
    lockAmount = Btoi(Extract(lockStateMaybe.value(), Int(24), Int(8)))

    return Seq([
        lockStateMaybe,
        Assert(lockStateMaybe.hasValue()),
        Assert(Global.latest_timestamp() >= lockEndDate),
        If(lockAssetId == Int(0)).Then(Seq([
            InnerTxnBuilder.Begin(),
            InnerTxnBuilder.SetField(TxnField.type_enum, TxnType.Payment),
            InnerTxnBuilder.SetField(TxnField.amount, lockAmount),
            InnerTxnBuilder.SetField(TxnField.receiver, Txn.sender()),
            InnerTxnBuilder.SetField(TxnField.fee, Int(0)),
            InnerTxnBuilder.Submit()
        ])).Else(Seq([
            InnerTxnBuilder.Begin(),
            InnerTxnBuilder.SetField(
                TxnField.type_enum, TxnType.AssetTransfer),
            InnerTxnBuilder.SetField(TxnField.xfer_asset, lockAssetId),
            InnerTxnBuilder.SetField(TxnField.asset_amount, lockAmount),
            InnerTxnBuilder.SetField(TxnField.asset_receiver, Txn.sender()),
            InnerTxnBuilder.SetField(TxnField.fee, Int(0)),
            InnerTxnBuilder.Submit()
        ])),
        App.localDel(Txn.sender(), lockId),
        Approve(),
    ])


def extendLock():
    lockId = Txn.application_args[1]
    unlockDate = Txn.application_args[2]
    existingLock = App.localGetEx(Txn.sender(), Txn.application_id(), lockId)
    existingLockEndDate = Btoi(Extract(existingLock.value(), Int(0), Int(8)))
    existingLockDateAssetIdAndAmount = Substring(
        existingLock.value(), Int(8), Int(32))

    return Seq([
        existingLock,
        Assert(existingLock.hasValue()),
        Assert(Btoi(unlockDate) > existingLockEndDate),
        App.localPut(Txn.sender(), lockId, Concat(
            unlockDate,
            existingLockDateAssetIdAndAmount,
        )),
        Approve(),
    ])


def escrow():

    neverAllowedConditions = [
        Txn.on_completion() == OnComplete.DeleteApplication,
        Txn.on_completion() == OnComplete.UpdateApplication,
    ]

    return Cond(
        [Or(*neverAllowedConditions), Reject()],

        [Txn.application_id() == Int(0), Approve()],
        [Txn.on_completion() == OnComplete.OptIn, Approve()],
        [Txn.application_args[0] == Bytes("lock"), createLock()],
        [Txn.application_args[0] == Bytes("add"), addToLock()],
        [Txn.application_args[0] == Bytes("extend"), extendLock()],
        [Txn.application_args[0] == Bytes("exit"), exitLock()],
        [Int(1), Reject()]
    )


if __name__ == "__main__":
    params = {}

    # Overwrite params if sys.argv[1] is passed
    if(len(sys.argv) > 1):
        params = parse_params(sys.argv[1], params)

    print(compileTeal(escrow(), Mode.Application, version=5))
