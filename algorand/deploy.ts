import { Algodv2, getApplicationAddress, Indexer, mnemonicToSecretKey } from 'algosdk';
import { deployTimelock, sendAlgos } from '../deployer/deploy';

const minAccountBalance = 1e5;

const algod = new Algodv2(
    'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa',
    'http://localhost',
    4001,
);
const indexer = new Indexer(
    'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa',
    'http://localhost',
    8980,
);
const defaultMnemonic = 'enforce drive foster uniform cradle tired win arrow wasp melt cattle chronic sport dinosaur announce shell correct shed amused dismiss mother jazz task above hospital';
const master = mnemonicToSecretKey(defaultMnemonic);

(async () => {
    const timelockAppId = await deployTimelock(algod, indexer, master);
    const timelockAddr = getApplicationAddress(timelockAppId);
    await sendAlgos(algod, master, timelockAddr, minAccountBalance);

    console.log('Timelock deployed and funded: ', timelockAppId);
})();