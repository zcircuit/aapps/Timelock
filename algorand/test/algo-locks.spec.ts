import algosdk, { Account, Algodv2 } from "algosdk";
import { algos, assertRejected, fees, IUtils, retryable, seconds, tick } from "./utils";
import { TimelockSdk } from '@zcircuit/timelock';
import { assert } from "chai";

const utils = new IUtils();
describe('[Integration] aEscrow with algos', function () {
    let accounts: Account[];
    let escrowAppId: number;
    let escrowAddr: string;
    let sdk: TimelockSdk;
    this.timeout(Infinity);

    beforeEach(async () => {
        accounts = await utils.createFundedAccounts(3);

        const masterAccount = accounts.slice(-1)[0];
        escrowAppId = await utils.deployEscrow(masterAccount);
        escrowAddr = algosdk.getApplicationAddress(escrowAppId);
        await utils.sendAlgos(masterAccount, escrowAddr, algos(1));

        sdk = new TimelockSdk(
            algosdk,
            utils.algod,
            utils.indexer,
            async (txns) => txns.map(tx =>
                tx.signTxn(accounts.find(account =>
                    account.addr === algosdk.encodeAddress(tx.from.publicKey))!.sk)),
        );
    });

    it('should lock funds until after the lock has expired', async () => {
        const address = accounts[0].addr;

        const beforeLockBalance = await utils.lookupBalance(address);
        await sdk.optIn(address, [escrowAppId]);

        const unlockDate = Date.now() + seconds(10);
        await sdk.lock({
            account: address,
            amount: algos(1),
            appId: BigInt(escrowAppId),
            lockId: 'lock-1',
            unlockDateMs: unlockDate,
        });

        const expectedAfterLockBalance = beforeLockBalance - algos(1) - fees(3);

        // Verify Balance After Lock
        await retryable(5, async () => {
            const afterLockBalance = await utils.lookupBalance(address);

            assert.closeTo(afterLockBalance, expectedAfterLockBalance, 999);
        });
        await retryable(5, async () => {
            const afterLockEscrowBalance = await utils.lookupBalance(escrowAddr);
            const initialBalance = algos(1);

            assert.closeTo(afterLockEscrowBalance, initialBalance + algos(1), 999);
        });

        await utils.waitForChainTimeAfter(unlockDate);

        await sdk.unlock({
            account: address,
            appId: BigInt(escrowAppId),
            lockId: 'lock-1',
        });

        // Verify Balance After Unlock
        await retryable(5, async () => {
            const afterUnlockBalance = await utils.lookupBalance(address);

            assert.closeTo(afterUnlockBalance, beforeLockBalance - fees(5), 999);
        });
    });

    it('should allow multiple distinct locks', async () => {
        const address = accounts[0].addr;
        const beforeLockBalance = await utils.lookupBalance(address);

        await sdk.optIn(address, [escrowAppId]);

        const unlockDate = Date.now() + seconds(10);
        await sdk.lock({
            account: address,
            amount: algos(1),
            appId: BigInt(escrowAppId),
            lockId: 'lock-1',
            unlockDateMs: unlockDate,
        });
        await sdk.lock({
            account: address,
            amount: algos(1),
            appId: BigInt(escrowAppId),
            lockId: 'lock-2',
            unlockDateMs: unlockDate + seconds(60),
        });

        await utils.waitForChainTimeAfter(unlockDate);

        await assertRejected(sdk.unlock({
            account: address,
            appId: BigInt(escrowAppId),
            lockId: 'lock-2'
        }), e => assert.include(e.message, 'assert failed'));

        await sdk.unlock({
            account: address,
            appId: BigInt(escrowAppId),
            lockId: 'lock-1'
        });

        await retryable(5, async () => {
            const afterUnlockBalance = await utils.lookupBalance(address);

            assert.closeTo(afterUnlockBalance, (beforeLockBalance - algos(1)) - fees(7), 999);
        });
    });

    it('should keep users escrows distinct', async () => {
        const address0 = accounts[0].addr;
        const address1 = accounts[1].addr;
        const beforeLockBalance0 = await utils.lookupBalance(address0);
        const beforeLockBalance1 = await utils.lookupBalance(address1);

        await Promise.all([
            sdk.optIn(address0, [escrowAppId]),
            sdk.optIn(address1, [escrowAppId]),
        ]);

        const unlockDate = Date.now() + seconds(10);
        await Promise.all([
            sdk.lock({
                account: address0,
                amount: algos(1),
                appId: BigInt(escrowAppId),
                lockId: 'lock-1',
                unlockDateMs: unlockDate,
            }),
            sdk.lock({
                account: address1,
                amount: algos(2),
                appId: BigInt(escrowAppId),
                lockId: 'lock-1',
                unlockDateMs: unlockDate,
            })
        ])

        await utils.waitForChainTimeAfter(unlockDate);

        await Promise.all([
            sdk.unlock({
                account: address0,
                appId: BigInt(escrowAppId),
                lockId: 'lock-1'
            }),
            sdk.unlock({
                account: address1,
                appId: BigInt(escrowAppId),
                lockId: 'lock-1'
            }),
        ])

        await retryable(5, async () => {
            const [afterUnlockBalance0, afterUnlockBalance1] = await Promise.all([
                utils.lookupBalance(address0),
                utils.lookupBalance(address1),
            ]);

            assert.closeTo(afterUnlockBalance0, beforeLockBalance0 - fees(5), 999);
            assert.closeTo(afterUnlockBalance1, beforeLockBalance1 - fees(5), 999);
        });
    });

});

