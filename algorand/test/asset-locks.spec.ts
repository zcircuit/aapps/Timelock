import algosdk, { Account } from "algosdk";
import { algos, fees, IUtils, retryable, seconds } from "./utils";
import { TimelockSdk } from '@zcircuit/timelock';
import { assert } from "chai";

const utils = new IUtils();

describe('[Integration] aEscrow with ASAs', function () {
    let accounts: Account[];
    let assetId: number;
    let escrowAppId: number;
    let escrowAddr: string;
    let sdk: TimelockSdk;
    this.timeout(Infinity);

    beforeEach(async () => {
        accounts = await utils.createFundedAccounts(3);

        const masterAccount = accounts.slice(-1)[0];
        escrowAppId = await utils.deployEscrow(masterAccount);
        escrowAddr = algosdk.getApplicationAddress(escrowAppId);
        await utils.sendAlgos(masterAccount, escrowAddr, algos(1));

        sdk = new TimelockSdk(
            algosdk,
            utils.algod,
            utils.indexer,
            async (txns) => txns.map(tx =>
                tx.signTxn(accounts.find(account =>
                    account.addr === algosdk.encodeAddress(tx.from.publicKey))!.sk)),
        );

        [assetId] = await utils.createTestingASAs([masterAccount]);
        await utils.optInAssetIfNecessary(accounts[0], assetId);
        await utils.optInAssetIfNecessary(accounts[1], assetId);
        await utils.sendAsset(assetId, masterAccount, accounts[0].addr, 1e6);
        await utils.sendAsset(assetId, masterAccount, accounts[1].addr, 1e6);
    });

    it('should be able to lock an ASA in the aEscrow that ssc has not opted into', async () => {
        const address = accounts[0].addr;

        const beforeLockBalance = await utils.lookupBalance(address);
        await sdk.optIn(address, [escrowAppId]);
        const unlockDate = Date.now() + seconds(10);
        await sdk.lock({
            account: address,
            amount: 5,
            appId: BigInt(escrowAppId),
            lockId: 'lock-1',
            unlockDateMs: unlockDate,
            assetId,
        });

        const expectedAfterLockBalance = beforeLockBalance - algos(.1) - fees(5);
        await retryable(5, async () => {
            const afterLockBalance = await utils.lookupBalance(address);
            const afterLockAssetBalance = await utils.lookupBalance(address, assetId);

            assert.closeTo(afterLockBalance, expectedAfterLockBalance, 999);
            assert.equal(afterLockAssetBalance, 1e6 - 5);
        });

        await utils.waitForChainTimeAfter(unlockDate);

        await sdk.unlock({
            account: address,
            appId: BigInt(escrowAppId),
            lockId: 'lock-1',
            assetId,
        });

        // Verify Balance After Unlock
        await retryable(5, async () => {
            const afterUnlockBalance = await utils.lookupBalance(address);
            const afterLockAssetBalance = await utils.lookupBalance(address, assetId);

            assert.closeTo(afterUnlockBalance, beforeLockBalance - algos(.1) - fees(7), 999);
            assert.equal(afterLockAssetBalance, 1e6);
        });
    });

    it('should be able to lock an ASA in the aEscrow that ssc has opted into', async () => {
        const address1 = accounts[0].addr;
        const address2 = accounts[1].addr;

        const beforeLockBalance = await utils.lookupBalance(address2);
        await Promise.all([
            sdk.optIn(address1, [escrowAppId]),
            sdk.optIn(address2, [escrowAppId]),
        ])
        const unlockDate = Date.now() + seconds(10);
        await sdk.lock({
            account: address1,
            amount: 5,
            appId: BigInt(escrowAppId),
            lockId: 'lock-1',
            unlockDateMs: unlockDate,
            assetId,
        });

        // Need to allow time for the indexer to catchup
        await retryable(5, async () => {
            const needsOptIn = await sdk.needsOptIn(BigInt(escrowAppId), BigInt(assetId));
            assert.ok(!needsOptIn);
        });

        await sdk.lock({
            account: address2,
            amount: 5,
            appId: BigInt(escrowAppId),
            lockId: 'lock-1',
            unlockDateMs: unlockDate,
            assetId,
        });

        // Note: address2 wont pay opt-in fees
        const expectedAfterLockBalance = beforeLockBalance - fees(3);
        await retryable(5, async () => {
            const afterLockBalance = await utils.lookupBalance(address2);
            const afterLockAssetBalance = await utils.lookupBalance(address2, assetId);

            assert.closeTo(afterLockBalance, expectedAfterLockBalance, 999);
            assert.equal(afterLockAssetBalance, 1e6 - 5);
        });

        await utils.waitForChainTimeAfter(unlockDate);

        await Promise.all([
            sdk.unlock({
                account: address1,
                appId: BigInt(escrowAppId),
                lockId: 'lock-1',
                assetId,
            }),
            sdk.unlock({
                account: address2,
                appId: BigInt(escrowAppId),
                lockId: 'lock-1',
                assetId,
            }),
        ])

        // Verify Balance After Unlock
        await retryable(5, async () => {
            const afterUnlockBalance = await utils.lookupBalance(address2);
            const afterLockAssetBalance = await utils.lookupBalance(address2, assetId);

            assert.closeTo(afterUnlockBalance, beforeLockBalance - fees(5), 999);
            assert.equal(afterLockAssetBalance, 1e6);
        });
    });

    it('should not require optin fees when ssc balance for the asset is zero', async () => {
        const address = accounts[0].addr;

        const beforeLockBalance = await utils.lookupBalance(address);
        await sdk.optIn(address, [escrowAppId]);

        // First txn to optin to asset
        const unlockDate = Date.now() + seconds(10);
        await sdk.lock({
            account: address,
            amount: 0,
            appId: BigInt(escrowAppId),
            lockId: 'lock-1',
            unlockDateMs: unlockDate,
            assetId,
        });

        const expectedAfterLockBalance = beforeLockBalance - algos(.1) - fees(5);
        await retryable(5, async () => {
            const afterLockBalance = await utils.lookupBalance(address);

            assert.closeTo(afterLockBalance, expectedAfterLockBalance, 999);
        });

        // Second txn that shouldn't require optin
        await sdk.lock({
            account: address,
            amount: 5,
            appId: BigInt(escrowAppId),
            lockId: 'lock-2',
            unlockDateMs: unlockDate,
            assetId,
        });

        // Verify Balance After 2 locks, one paying optin fees
        await retryable(5, async () => {
            const afterUnlockBalance = await utils.lookupBalance(address);

            assert.closeTo(afterUnlockBalance, beforeLockBalance - algos(.1) - fees(7), 999);
        });
    });

    // I should be able to add funds to a lock
    // I should be able to add funds to one of multiple escrows

});
