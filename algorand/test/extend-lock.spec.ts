import algosdk, { Account, Algodv2 } from "algosdk";
import { algos, assertRejected, fees, IUtils, retryable, seconds, tick } from "./utils";
import { TimelockSdk } from '@zcircuit/timelock'; // Use this import to test published artifacts
// import { TimelockSdk } from '../../sdk/lib/index'; // Use this import for local dev
import { assert } from "chai";

const utils = new IUtils();
describe('[Integration] aEscrow with an extended lock duration', function () {
    let accounts: Account[];
    let escrowAppId: number;
    let escrowAddr: string;
    let sdk: TimelockSdk;
    this.timeout(Infinity);

    beforeEach(async () => {
        accounts = await utils.createFundedAccounts(3);

        const masterAccount = accounts.slice(-1)[0];
        escrowAppId = await utils.deployEscrow(masterAccount);
        escrowAddr = algosdk.getApplicationAddress(escrowAppId);
        await utils.sendAlgos(masterAccount, escrowAddr, algos(1));

        sdk = new TimelockSdk(
            algosdk,
            utils.algod,
            utils.indexer,
            async (txns) => txns.map(tx =>
                tx.signTxn(accounts.find(account =>
                    account.addr === algosdk.encodeAddress(tx.from.publicKey))!.sk)),
        );
    });

    it('should lock funds until after the extended lock has expired', async () => {
        const address = accounts[0].addr;

        const beforeLockBalance = await utils.lookupBalance(address);
        await sdk.optIn(address, [escrowAppId]);

        const unlockDate = Date.now() + seconds(10);
        await sdk.lock({
            account: address,
            amount: algos(1),
            appId: BigInt(escrowAppId),
            lockId: 'lock-1',
            unlockDateMs: unlockDate,
        });

        const expectedAfterLockBalance = beforeLockBalance - algos(1) - fees(3);

        // Verify Balance After Lock
        await retryable(5, async () => {
            const afterLockBalance = await utils.lookupBalance(address);

            assert.closeTo(afterLockBalance, expectedAfterLockBalance, 999);
        });
        await retryable(5, async () => {
            const afterLockEscrowBalance = await utils.lookupBalance(escrowAddr);
            const initialBalance = algos(1);

            assert.closeTo(afterLockEscrowBalance, initialBalance + algos(1), 999);
        });

        const extendedUnlockDate = unlockDate + seconds(10);

        await sdk.extend({
            account: address,
            appId: BigInt(escrowAppId),
            lockId: 'lock-1',
            unlockDateMs: extendedUnlockDate,
        });

        await utils.waitForChainTimeAfter(extendedUnlockDate);

        await sdk.unlock({
            account: address,
            appId: BigInt(escrowAppId),
            lockId: 'lock-1',
        });

        // Verify Balance After Unlock
        await retryable(5, async () => {
            const afterUnlockBalance = await utils.lookupBalance(address);

            assert.closeTo(afterUnlockBalance, beforeLockBalance - fees(6), 999);
        });
    });

    it('should not allow withdrawing at the previous unlock date when a lock is extended', async () => {
        const address = accounts[0].addr;
        await sdk.optIn(address, [escrowAppId]);
        const unlockDate = Date.now() + seconds(10);
        await sdk.lock({
            account: address,
            amount: algos(1),
            appId: BigInt(escrowAppId),
            lockId: 'lock-1',
            unlockDateMs: unlockDate,
        });

        const extendedUnlockDate = unlockDate + seconds(10);
        await sdk.extend({
            account: address,
            appId: BigInt(escrowAppId),
            lockId: 'lock-1',
            unlockDateMs: extendedUnlockDate,
        });

        // Only wait for the first (non-extended) lock to expire
        await utils.waitForChainTimeAfter(unlockDate);

        await assertRejected(sdk.unlock({
            account: address,
            appId: BigInt(escrowAppId),
            lockId: 'lock-1',
        }), e => assert.include(e.message, 'assert failed'));
    });

    it('should only allow an extension beyond the previous lock end date', async () => {
        const address = accounts[0].addr;
        await sdk.optIn(address, [escrowAppId]);
        const unlockDate = Date.now() + seconds(10);
        await sdk.lock({
            account: address,
            amount: algos(1),
            appId: BigInt(escrowAppId),
            lockId: 'lock-1',
            unlockDateMs: unlockDate,
        });

        const extendedUnlockDate = unlockDate - seconds(1);
        await assertRejected(sdk.extend({
            account: address,
            appId: BigInt(escrowAppId),
            lockId: 'lock-1',
            unlockDateMs: extendedUnlockDate,
        }), e => assert.include(e.message, 'assert failed'));
    });
});

