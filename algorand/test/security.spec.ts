import { Account, Transaction } from "algosdk";
const algosdk = require('algosdk');
import { algos, assertRejected, fees, IUtils, seconds, textToUint8 } from "./utils";
import { TimelockSdk } from '@zcircuit/timelock';
import { assert } from "chai";

const utils = new IUtils();
describe('[Integration] aEscrow Security Edge Cases', function () {
    let accounts: Account[];
    let escrowAppId: number;
    let escrowAddr: string;
    let sdk: TimelockSdk;
    this.timeout(Infinity);

    beforeEach(async () => {
        accounts = await utils.createFundedAccounts(3);

        const masterAccount = accounts.slice(-1)[0];
        escrowAppId = await utils.deployEscrow(masterAccount);
        escrowAddr = algosdk.getApplicationAddress(escrowAppId);
        await utils.sendAlgos(masterAccount, escrowAddr, algos(1));

        sdk = new TimelockSdk(
            algosdk,
            utils.algod,
            utils.indexer,
            async (txns) => txns.map(tx =>
                tx.signTxn(accounts.find(account =>
                    account.addr === algosdk.encodeAddress(tx.from.publicKey))!.sk)),
        );
    });

    it('should not allow an unlock before the lock has expired', async () => {
        const address = accounts[0].addr;

        await sdk.optIn(address, [escrowAppId]);

        const unlockDate = Date.now() + seconds(10);
        await sdk.lock({
            account: address,
            amount: algos(1),
            appId: BigInt(escrowAppId),
            lockId: 'lock-1',
            unlockDateMs: unlockDate,
        });

        await assertRejected(sdk.unlock({
            account: address,
            appId: BigInt(escrowAppId),
            lockId: 'lock-1'
        }), e => assert.include(e.message, 'assert failed'));
    });

    it('should reject lock creation when an algo payment is to an address other than the escrow', async () => {
        const address = accounts[0].addr;
        const account = accounts[0];
        await sdk.optIn(address, [escrowAppId]);
        const suggestedParams = await utils.algod.getTransactionParams().do();

        const escrowTx = algosdk.makeApplicationNoOpTxnFromObject({
            suggestedParams,
            from: account.addr,
            appIndex: escrowAppId, // TODO: why no bigint?,
            appArgs: [
                textToUint8('lock'),
                textToUint8('lock-1'),
                algosdk.encodeUint64(Math.floor((Date.now() + seconds(1)) / 1e3)),
            ]
        });
        const paymentTx = algosdk.makePaymentTxnWithSuggestedParamsFromObject({
            suggestedParams,
            from: address,
            amount: algos(1),
            // Had lots of trouble here, account must exist on the chain or else keep getting a 
            // 'one or more signatures did not pass verification' which results in a false-positive for this test
            to: "WWYNX3TKQYVEREVSW6QQP3SXSFOCE3SKUSEIVJ7YAGUPEACNI5UGI4DZCE",
        });

        const txns: Transaction[] = algosdk.assignGroupID([escrowTx, paymentTx]);
        const signedGroupTx = txns.map(tx => tx.signTxn(account.sk));

        await assertRejected(utils.algod.sendRawTransaction(signedGroupTx).do(), e => {
            console.log('Received Expected Error:', e.message);
            assert.include(e.message, 'assert failed');
            assert.include(e.message, 'global CurrentApplicationAddress\n==\nassert')
        });
    });

    describe('with ASAs', () => {
        let assetId: number;

        beforeEach(async () => {
            const masterAccount = accounts.slice(-1)[0];
            [assetId] = await utils.createTestingASAs([masterAccount]);
            await utils.optInAssetIfNecessary(accounts[0], assetId);
            await utils.sendAsset(assetId, masterAccount, accounts[0].addr, 1e6);
        });

        it('should not allow an escrow when optIn payment is omitted', async () => {
            const account = accounts[0];
            const address = account.addr;

            await sdk.optIn(address, [escrowAppId]);
            const suggestedParams = await utils.algod.getTransactionParams().do();

            const escrowTx = algosdk.makeApplicationNoOpTxnFromObject({
                suggestedParams,
                from: account.addr,
                appIndex: escrowAppId, // TODO: why no bigint?,
                appArgs: [
                    textToUint8('lock'),
                    textToUint8('lock-1'),
                    algosdk.encodeUint64(Math.floor((Date.now() + seconds(1)) / 1e3)),
                ],
                foreignAssets: [assetId]
            });
            const paymentTx = algosdk.makeAssetTransferTxnWithSuggestedParamsFromObject({
                suggestedParams,
                from: address,
                amount: 5,
                to: escrowAddr,
                assetIndex: assetId,
            });

            const txns: Transaction[] = algosdk.assignGroupID([escrowTx, paymentTx]);
            const signedGroupTx = txns.map(tx => tx.signTxn(account.sk));

            await assertRejected(utils.algod.sendRawTransaction(signedGroupTx).do(), e => {
                console.log('Received Expected Error:', e.message);
                assert.include(e.message, '- would result negative');
            });
        });

        it('should not allow an escrow when optIn payment is to address other than escrow', async () => {
            const account = accounts[0];
            const address = account.addr;

            await sdk.optIn(address, [escrowAppId]);
            const suggestedParams = await utils.algod.getTransactionParams().do();

            const escrowTx = algosdk.makeApplicationNoOpTxnFromObject({
                suggestedParams,
                from: account.addr,
                appIndex: escrowAppId, // TODO: why no bigint?,
                appArgs: [
                    textToUint8('lock'),
                    textToUint8('lock-1'),
                    algosdk.encodeUint64(Math.floor((Date.now() + seconds(1)) / 1e3)),
                ],
                foreignAssets: [assetId]
            });
            const paymentTx = algosdk.makeAssetTransferTxnWithSuggestedParamsFromObject({
                suggestedParams,
                from: address,
                amount: 5,
                to: escrowAddr,
                assetIndex: assetId,
            });
            const optInPaymentTx = algosdk.makePaymentTxnWithSuggestedParamsFromObject({
                suggestedParams: {
                    ...suggestedParams,
                    flatFee: true,
                    fee: Math.max(suggestedParams.fee, 1e3) * 2, //Addtn'l fee so the escrow can send a txn
                },
                from: address,
                amount: .1 * 1e6,
                to: 'WWYNX3TKQYVEREVSW6QQP3SXSFOCE3SKUSEIVJ7YAGUPEACNI5UGI4DZCE',
            });

            const txns: Transaction[] = algosdk.assignGroupID([optInPaymentTx, escrowTx, paymentTx,]);
            const signedGroupTx = txns.map(tx => tx.signTxn(account.sk));

            await assertRejected(utils.algod.sendRawTransaction(signedGroupTx).do(), e => {
                console.log('Received Expected Error:', e.message);
                assert.include(e.message, 'assert failed');
            });
        });

        it('should not allow an escrow when optIn payment is not .1 algos to pay the min balance increase', async () => {
            const account = accounts[0];
            const address = account.addr;

            await sdk.optIn(address, [escrowAppId]);
            const suggestedParams = await utils.algod.getTransactionParams().do();

            const escrowTx = algosdk.makeApplicationNoOpTxnFromObject({
                suggestedParams,
                from: account.addr,
                appIndex: escrowAppId, // TODO: why no bigint?,
                appArgs: [
                    textToUint8('lock'),
                    textToUint8('lock-1'),
                    algosdk.encodeUint64(Math.floor((Date.now() + seconds(1)) / 1e3)),
                ],
                foreignAssets: [assetId]
            });
            const paymentTx = algosdk.makeAssetTransferTxnWithSuggestedParamsFromObject({
                suggestedParams,
                from: address,
                amount: 5,
                to: escrowAddr,
                assetIndex: assetId,
            });
            const optInPaymentTx = algosdk.makePaymentTxnWithSuggestedParamsFromObject({
                suggestedParams: {
                    ...suggestedParams,
                    flatFee: true,
                    fee: Math.max(suggestedParams.fee, 1e3) * 2, //Addtn'l fee so the escrow can send a txn
                },
                from: address,
                amount: .01 * 1e6,
                to: escrowAddr,
            });

            const txns: Transaction[] = algosdk.assignGroupID([optInPaymentTx, escrowTx, paymentTx,]);
            const signedGroupTx = txns.map(tx => tx.signTxn(account.sk));

            await assertRejected(utils.algod.sendRawTransaction(signedGroupTx).do(), e => {
                console.log('Received Expected Error:', e.message);
                assert.include(e.message, 'assert failed');
            });
        });
    });

    describe('InnerTxn fees', () => {

        it('unlock iTxn should have a fee of 0, requiring extra user supplied algos in the fee pool', async () => {
            const address = accounts[0].addr;

            await sdk.optIn(address, [escrowAppId]);

            const unlockDate = Date.now() + seconds(10);
            await sdk.lock({
                account: address,
                amount: algos(1),
                appId: BigInt(escrowAppId),
                lockId: 'lock-1',
                unlockDateMs: unlockDate,
            });

            await utils.waitForChainTimeAfter(unlockDate);

            const suggestedParams = await utils.algod.getTransactionParams().do();
            const unlockTx: Transaction = algosdk.makeApplicationNoOpTxnFromObject({
                suggestedParams: {
                    ...suggestedParams,
                    flatFee: true,
                    fee: fees(1),
                },
                from: address,
                appIndex: Number(escrowAppId), // TODO: why no bigint?,
                appArgs: [
                    textToUint8('exit'),
                    textToUint8('lock-1'),
                ]
            });
            const signedTx = unlockTx.signTxn(accounts[0].sk);

            await assertRejected(utils.algod.sendRawTransaction(signedTx).do(), e => {
                assert.include(e.message, 'logic eval error: fee too small');
            });
        });

        it('asset optIn iTxn should have a fee of 0, requiring extra user supplied algos in the fee pool', async () => {
            const masterAccount = accounts.slice(-1)[0];
            const address = accounts[0].addr;
            const [assetId] = await utils.createTestingASAs([masterAccount]);

            await utils.optInAssetIfNecessary(accounts[0], assetId);
            await utils.sendAsset(assetId, masterAccount, accounts[0].addr, 1e6);
            await sdk.optIn(address, [escrowAppId]);

            const suggestedParams = await utils.algod.getTransactionParams().do();

            const escrowTx = algosdk.makeApplicationNoOpTxnFromObject({
                suggestedParams,
                from: address,
                appIndex: escrowAppId, // TODO: why no bigint?,
                appArgs: [
                    textToUint8('lock'),
                    textToUint8('lock-1'),
                    algosdk.encodeUint64(Math.floor((Date.now() + seconds(1)) / 1e3)),
                ],
                foreignAssets: [assetId]
            });
            const paymentTx = algosdk.makeAssetTransferTxnWithSuggestedParamsFromObject({
                suggestedParams,
                from: address,
                amount: 5,
                to: escrowAddr,
                assetIndex: assetId,
            });
            const optInPaymentTx = algosdk.makePaymentTxnWithSuggestedParamsFromObject({
                suggestedParams: {
                    ...suggestedParams,
                    flatFee: true,
                    fee: fees(1), //Should require 2 fees for optin itxn
                },
                from: address,
                amount: algos(.1),
                to: escrowAddr,
            });

            const txns: Transaction[] = algosdk.assignGroupID([optInPaymentTx, escrowTx, paymentTx,]);
            const signedGroupTx = txns.map(tx => tx.signTxn(accounts[0].sk));

            await assertRejected(utils.algod.sendRawTransaction(signedGroupTx).do(), e => {
                assert.include(e.message, 'logic eval error: fee too small');
            });
        });
    })
});
