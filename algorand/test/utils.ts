import {
    Account,
    Algodv2,
    generateAccount,
    Indexer,
    makeApplicationCreateTxnFromObject,
    makeAssetCreateTxnWithSuggestedParamsFromObject,
    makeAssetDestroyTxnWithSuggestedParams,
    makeAssetTransferTxn,
    makeAssetTransferTxnWithSuggestedParamsFromObject,
    makePaymentTxnWithSuggestedParamsFromObject,
    mnemonicToSecretKey,
    OnApplicationComplete,
    waitForConfirmation
} from "algosdk";
import { getProgram } from "@algo-builder/runtime";
import { CreateApplicationTransactionQuery } from "./types";

const clearProgram = getProgram('noop-clear.teal');
const escrowProgram = getProgram('escrow.py');

export class IUtils {

    algod: Algodv2;
    indexer: Indexer;

    private master: Account;

    constructor(algod?: Algodv2, indexer?: Indexer, mnemonic?: string) {
        this.algod = algod || new Algodv2(
            'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa',
            'http://localhost',
            4001,
        );
        this.indexer = indexer || new Indexer(
            'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa',
            'http://localhost',
            8980,
        );
        const defaultMnemonic = 'enforce drive foster uniform cradle tired win arrow wasp melt cattle chronic sport dinosaur announce shell correct shed amused dismiss mother jazz task above hospital';
        this.master = mnemonicToSecretKey(mnemonic || defaultMnemonic);
    }

    createFundedAccounts(num: number) {
        return Promise.all(Array.from({ length: num })
            .map(async () => {
                const acc = generateAccount();

                await this.sendAlgos(this.master, acc.addr, 10e6);
                return acc;
            })
        )
    }

    createTestingASAs(creators: Account[]) {
        return Promise.all(creators.map(async (account, i) => {
            const suggestedParams = await this.algod.getTransactionParams().do();
            const tx = makeAssetCreateTxnWithSuggestedParamsFromObject({
                decimals: 0,
                defaultFrozen: false,
                total: 1e6 * 10,
                from: account.addr,
                assetName: `ASA-${i}`,
                suggestedParams,
                // reserve: account.addr,
            })
            const signedTx = tx.signTxn(account.sk);
            await this.algod.sendRawTransaction(signedTx).do();
            await waitForConfirmation(this.algod, tx.txID(), 20);

            const createdAssetTransaction: any = await this.tryQueryForTransaction(tx.txID());
            const assetId = createdAssetTransaction.transaction["created-asset-index"];

            console.log('Asset Created', assetId);
            return assetId;
        }));
    }

    async sendAlgos(from: Account, toAddr: Account['addr'], amountMicroAlgos: number) {
        const suggestedParams = await this.algod.getTransactionParams().do();

        const tx = makePaymentTxnWithSuggestedParamsFromObject({
            amount: amountMicroAlgos,
            from: from.addr,
            to: toAddr,
            suggestedParams,
        });
        const signedTx = tx.signTxn(from.sk);
        await this.algod.sendRawTransaction(signedTx).do();
        await waitForConfirmation(this.algod, tx.txID(), 20);
    }

    async sendAsset(assetId: number, from: Account, toAddr: Account['addr'], amount: number) {
        const suggestedParams = await this.algod.getTransactionParams().do();

        const tx = makeAssetTransferTxnWithSuggestedParamsFromObject({
            assetIndex: assetId,
            amount,
            from: from.addr,
            to: toAddr,
            suggestedParams,
        });
        const signedTx = tx.signTxn(from.sk);
        await this.algod.sendRawTransaction(signedTx).do();
        await waitForConfirmation(this.algod, tx.txID(), 20);
    }

    async optInAssetIfNecessary(account: Account, assetId: number) {
        const result = await this.indexer.lookupAccountByID(account.addr).do();
        const hasAsset = (result.account.assets || []).some((t: any) => t['asset-id'] === assetId);
        if (hasAsset) return;

        await this.sendAsset(assetId, account, account.addr, 0);
        console.log(`${account.addr} opt-in to ${assetId}`);
    }

    async deployEscrow(creator: Account) {
        let suggestedParams = await this.algod.getTransactionParams().do();
        const compiledClearProgram = await (this.algod.compile(clearProgram).do() as CompileResult);
        const compiledEscrowProgram = await (this.algod.compile(escrowProgram).do() as CompileResult);

        const escrowTx = makeApplicationCreateTxnFromObject({
            approvalProgram: b64ToUint8(compiledEscrowProgram.result),
            clearProgram: b64ToUint8(compiledClearProgram.result),
            numGlobalInts: 0,
            numGlobalByteSlices: 0,
            numLocalInts: 0,
            numLocalByteSlices: 16,
            onComplete: OnApplicationComplete.NoOpOC,
            from: creator.addr,
            suggestedParams,
        });

        const signedEscrowTx = escrowTx.signTxn(creator.sk);
        await this.algod.sendRawTransaction(signedEscrowTx).do();
        await waitForConfirmation(this.algod, escrowTx.txID(), 20);

        const createdEscrowTransaction: CreateApplicationTransactionQuery = await this.tryQueryForTransaction(escrowTx.txID());
        const escrowAppId = createdEscrowTransaction.transaction["created-application-index"];
        console.log(`Escrow Application created: ${escrowAppId}`);
        return escrowAppId;
    }

    async lookupBalance(address: string, assetId = 0) {
        const accountDetails = await this.indexer.lookupAccountByID(address).do();
        const asset = (accountDetails.account.assets || []).find((a: any) => a['asset-id'] === assetId);

        return !!assetId
            ? asset?.amount || 0
            : accountDetails.account.amount;
    }

    async waitForChainTimeAfter(timeMs: number, timeout = 30000) {
        const start = Date.now();
        const waitingTime = Math.floor(timeMs / 1e3);
        while (true) {
            const indexerStatus = await this.indexer.makeHealthCheck().do();
            const blockInfo = await this.indexer.lookupBlock(indexerStatus.round).do();
            const blockTime = blockInfo.timestamp;
            if (blockTime > waitingTime) break;
            if (Date.now() - start > timeout) throw new Error('TIMEOUT');
            await tick(2000);
        }
    }

    private async tryQueryForTransaction(txid: string, retries = 10) {
        let i = 0;
        let lastError: any;
        while (i < retries) {
            const createdTransaction: CreateApplicationTransactionQuery | null = await this.indexer.lookupTransactionByID(txid).do()
                .catch(e => {
                    lastError = e;
                    return null;
                }) as any;

            if (createdTransaction) return createdTransaction;
            await tick(1000);
            i++;
        }

        throw new Error(lastError.toString());
    }
}

export function tick(time: number) {
    return new Promise(res => setTimeout(res, time));
}

export const atob = globalThis.atob || ((src: string) => {
    return Buffer.from(src, 'base64').toString('binary');
})

export const btoa = globalThis.btoa || ((src: string) => {
    return Buffer.from(src, 'binary').toString('base64');
})

export function b64ToUint8(b64: string): Uint8Array {
    return Uint8Array.from(atob(b64), c => c.charCodeAt(0));
}

export function textToUint8(text: string): Uint8Array {
    return Uint8Array.from(text, c => c.charCodeAt(0));
}

export function uint8ToText(bytes: Uint8Array): string {
    return Array.from(bytes).map(b => String.fromCharCode(b)).join('');
}

export function uint8ToB64(bytes: Uint8Array): string {
    return btoa(uint8ToText(bytes));
}

export function fees(numTxs: number) {
    return numTxs * 1e3;
}

export function algos(numAlgos: number) {
    return numAlgos * 1e6; //microAlgos
}

export function seconds(numSeconds: number) {
    return numSeconds * 1e3; //milliSeconds
}

export async function retryable<T>(tries: number, fn: () => Promise<T>): Promise<T> {
    let iter = 0;
    let err;
    let result: T = null as any;

    while (iter < tries) {
        iter++;
        err = null;

        result = await fn().catch(e => err = e);
        if (err) {
            await tick(1000);
        } else {
            break;
        };
    }

    if (err) throw err;

    return result;
}

const defaultWithError = () => { }
export async function assertRejected(promise: Promise<unknown>, withError: (e: Error) => void = defaultWithError) {
    let caught = false;

    await promise.catch(e => {
        caught = true;
        withError(e);
    });

    if (!caught) throw new Error('Expected a rejection');
}

export type CompileResult = Promise<{
    hash: string,
    result: string; //base64 encoded
}>
