import { getProgram } from "@algo-builder/runtime";
import { Algodv2 } from "algosdk";
import { mkdirp, writeJSON } from 'fs-extra';
import { join } from 'path';

// Algobuilder makes an assumption 'assets' is in cwd
process.chdir('../algorand');

const clearProgram = getProgram('noop-clear.teal');
const escrowProgram = getProgram('escrow.py');

// For now, only compile locally before publishing
const algod = new Algodv2(
    'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa',
    'http://localhost',
    4001,
);

(async () => {
    const distDir = join(__dirname, 'dist');
    const distFile = join(distDir, 'compiledTimelock.json');

    const compiledClearProgram = await (algod.compile(clearProgram).do() as CompileResult);
    const compiledEscrowProgram = await (algod.compile(escrowProgram).do() as CompileResult);

    await mkdirp(distDir);
    await writeJSON(distFile, {
        escrow: compiledEscrowProgram,
        clear: compiledClearProgram,
    });
})();

type CompileResult = Promise<{
    hash: string,
    result: string; //base64 encoded
}>
