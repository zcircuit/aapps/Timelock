import { Account, Algodv2, Indexer, makeApplicationCreateTxnFromObject, makeAssetTransferTxnWithSuggestedParamsFromObject, makePaymentTxnWithSuggestedParamsFromObject, OnApplicationComplete, SuggestedParams, waitForConfirmation } from "algosdk";
import { readJson, pathExists } from "fs-extra";
import { join } from 'path';

export async function getDeployTimelockTransaction(suggestedParams: SuggestedParams, creatorAddr: string) {
    const compiledTimelockFile1 = join(__dirname, './dist/compiledTimelock.json');

    // Once packaged the filepath is slightly different
    const compiledTimelockFile2 = join(__dirname, './compiledTimelock.json');
    const actualFile = await pathExists(compiledTimelockFile1) ? compiledTimelockFile1 : compiledTimelockFile2;

    const { escrow, clear } = await readJson(actualFile)

    return makeApplicationCreateTxnFromObject({
        approvalProgram: b64ToUint8(escrow.result),
        clearProgram: b64ToUint8(clear.result),
        numGlobalInts: 0,
        numGlobalByteSlices: 0,
        numLocalInts: 0,
        numLocalByteSlices: 16,
        onComplete: OnApplicationComplete.NoOpOC,
        from: creatorAddr,
        suggestedParams,
    });
}

export async function deployTimelock(algod: Algodv2, indexer: Indexer, creator: Account) {
    const suggestedParams = await algod.getTransactionParams().do();
    const escrowTx = await getDeployTimelockTransaction(suggestedParams, creator.addr);

    const signedEscrowTx = escrowTx.signTxn(creator.sk);
    await algod.sendRawTransaction(signedEscrowTx).do();
    await waitForConfirmation(algod, escrowTx.txID(), 20);

    const createdEscrowTransaction = await tryQueryForTransaction(indexer, escrowTx.txID());
    const escrowAppId = createdEscrowTransaction.transaction["created-application-index"];
    console.log(`Escrow Application created: ${escrowAppId}, be sure to fund the escrow to maintain minimum balance`);
    return BigInt(escrowAppId);
}

export async function sendAlgos(algod: Algodv2, from: Account, toAddr: Account['addr'], amountMicroAlgos: number) {
    const suggestedParams = await algod.getTransactionParams().do();

    const tx = makePaymentTxnWithSuggestedParamsFromObject({
        amount: amountMicroAlgos,
        from: from.addr,
        to: toAddr,
        suggestedParams,
    });
    const signedTx = tx.signTxn(from.sk);
    await algod.sendRawTransaction(signedTx).do();
    await waitForConfirmation(algod, tx.txID(), 20);
}

export async function sendAsset(algod: Algodv2, assetId: number, from: Account, toAddr: Account['addr'], amount: number) {
    const suggestedParams = await algod.getTransactionParams().do();

    const tx = makeAssetTransferTxnWithSuggestedParamsFromObject({
        assetIndex: assetId,
        amount,
        from: from.addr,
        to: toAddr,
        suggestedParams,
    });
    const signedTx = tx.signTxn(from.sk);
    await algod.sendRawTransaction(signedTx).do();
    await waitForConfirmation(algod, tx.txID(), 20);
}

const atob = (src: string) => Buffer.from(src, 'base64').toString('binary');
function b64ToUint8(b64: string): Uint8Array {
    return Uint8Array.from(atob(b64), c => c.charCodeAt(0));
}
function tick(time: number) {
    return new Promise(res => setTimeout(res, time));
}

async function tryQueryForTransaction(indexer: Indexer, txid: string, retries = 10) {
    let i = 0;
    let lastError: any;
    while (i < retries) {
        const createdTransaction = await indexer.lookupTransactionByID(txid).do()
            .catch(e => {
                lastError = e;
                return null;
            }) as any;

        if (createdTransaction) return createdTransaction;
        await tick(1000);
        i++;
    }

    throw new Error(lastError.toString());
}