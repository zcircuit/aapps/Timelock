import type { Algodv2, Indexer, Transaction, SuggestedParams } from 'algosdk';
import { atob, b64ToUint8, textToUint8 } from './utils';
import { AlgoSdk } from './algosdk';
import { TimelockTransactions, ExtendConfig, LockConfig, UnlockConfig } from './transactions';

export class TimelockSdk {

    transactions = new TimelockTransactions(this.algosdk);

    constructor(
        private algosdk: AlgoSdk,
        private algod: Algodv2,
        private indexer: Indexer,
        private signer: (txns: Transaction[]) => Promise<Uint8Array[]>,
    ) { }

    optIn(account: string, appIds: number[]) {
        return this.withErrorHandling('[timelock: optIn]', async () => {
            const suggestedParams = await this.algod.getTransactionParams().do();
            const txs = appIds.map(appId => this.transactions.optIn(suggestedParams, account, appId))
            const signedOptInTxs = await this.signer(txs);
            await this.algod.sendRawTransaction(signedOptInTxs).do();
            await this.algosdk.waitForConfirmation(this.algod, txs[0].txID(), 20);
        });
    }

    lock(config: LockConfig) {
        return this.withErrorHandling('[timelock: lock]', async () => {
            const needsOptIn = config.assetId && await this.needsOptIn(config.appId, BigInt(config.assetId));

            const suggestedParams = await this.algod.getTransactionParams().do();

            let txns: Transaction[] = this.transactions.lock(suggestedParams, config);
            if (needsOptIn) {
                txns = [this.transactions.additionalPaymentForEscrowOptIn(suggestedParams, config), ...txns];
            }

            const signedGroupTx = await this.signer(this.algosdk.assignGroupID(txns));

            await this.algod.sendRawTransaction(signedGroupTx).do();
            await this.algosdk.waitForConfirmation(this.algod, txns[0].txID(), 20);
        });
    }

    unlock(config: UnlockConfig) {
        return this.withErrorHandling('[timelock: unlock]', async () => {
            const suggestedParams = await this.algod.getTransactionParams().do();

            const escrowTx = this.transactions.unlock(suggestedParams, config);

            const signedTx = await this.signer([escrowTx]);
            await this.algod.sendRawTransaction(signedTx).do();
            await this.algosdk.waitForConfirmation(this.algod, escrowTx.txID(), 20);
        });
    }

    extend(config: ExtendConfig) {
        return this.withErrorHandling('[timelock: extend]', async () => {
            const suggestedParams = await this.algod.getTransactionParams().do();

            const escrowTx = this.transactions.extend(suggestedParams, config);

            const signedTx = await this.signer([escrowTx]);
            await this.algod.sendRawTransaction(signedTx).do();
            await this.algosdk.waitForConfirmation(this.algod, escrowTx.txID(), 20);
        });
    }

    getLocalState(appId: bigint, account: string) {
        return this.withErrorHandling('[timelock: getLocalState]', async () => {
            const accountRes = await this.indexer.lookupAccountByID(account).do();
            return this.parseLocalState(accountRes, appId);
        })
    }

    parseLocalState(accountRes: LookupAccountByIDResponse, appId: bigint) {
        const localStateRaw = (accountRes.account['apps-local-state'] || [])
            .find((s: { id?: number }) => s.id == Number(appId)) || {};

        const locks = Object.fromEntries((localStateRaw['key-value'] || []).map((entry: { key: string, value: any }) => {
            const lockId = atob(entry.key);
            const stateBytes = b64ToUint8(entry.value.bytes);
            const endDate = this.algosdk.decodeUint64(stateBytes.slice(0, 8), 'bigint');
            const startDate = this.algosdk.decodeUint64(stateBytes.slice(8, 16), 'bigint');
            const assetId = this.algosdk.decodeUint64(stateBytes.slice(16, 24), 'bigint');
            const amount = this.algosdk.decodeUint64(stateBytes.slice(24, 32), 'bigint');

            return [lockId, {
                endDate,
                startDate,
                assetId,
                amount,
            }]
        }));

        return locks as Record<string, {
            endDate: bigint,
            startDate: bigint,
            assetId: bigint,
            amount: bigint,
        }>
    }

    needsOptIn(appId: bigint, assetId: bigint) {
        return this.withErrorHandling('[timelock: needsOptIn]-', async () => {
            const escrowAddr = this.algosdk.getApplicationAddress(appId);
            const numAssetId = Number(assetId);

            const lookupResult = await this.indexer.lookupAccountByID(escrowAddr).do();
            return !((lookupResult.account.assets || []).some((t: any) => t['asset-id'] === numAssetId));
        });
    }

    private async withErrorHandling<T = unknown>(desc: string, todo: () => Promise<T>): Promise<T> {
        try {
            return await todo();
        } catch (e: any) {
            console.error(`Error:${desc}[${e.status}]`, e?.response?.text);
            throw e;
        }
    }
}

export type LookupAccountByIDResponse = any;
