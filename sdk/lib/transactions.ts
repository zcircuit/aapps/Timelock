import type { SuggestedParams, Transaction } from "algosdk";
import { AlgoSdk } from "./algosdk";
import { textToUint8 } from "./utils";

export class TimelockTransactions {
    constructor(
        private algosdk: AlgoSdk,
    ) { }

    optIn(suggestedParams: SuggestedParams, from: string, appIndex: number) {
        return this.algosdk.makeApplicationOptInTxnFromObject({
            suggestedParams,
            from,
            appIndex,
        });
    }

    additionalPaymentForEscrowOptIn(suggestedParams: SuggestedParams, config: LockConfig): ExtraPaymentTxn {
        const escrowAddr = this.algosdk.getApplicationAddress(config.appId);
        return this.algosdk.makePaymentTxnWithSuggestedParamsFromObject({
            suggestedParams: {
                ...suggestedParams,
                flatFee: true,
                fee: Math.max(suggestedParams.fee, 1e3) * 2, //Addtn'l fee so the escrow can send a txn
            },
            from: config.account,
            amount: .1 * 1e6,
            to: escrowAddr,
        })
    }

    lock(suggestedParams: SuggestedParams, config: LockConfig): [EscrowTxn, PaymentTxn] {
        const isAsa = !!config.assetId;
        const escrowTx = this.algosdk.makeApplicationNoOpTxnFromObject({
            suggestedParams,
            from: config.account,
            appIndex: Number(config.appId), // TODO: why no bigint?,
            appArgs: [
                textToUint8('lock'),
                typeof config.lockId == 'string' ? textToUint8(config.lockId) : config.lockId,
                this.algosdk.encodeUint64(Math.floor(config.unlockDateMs / 1e3)),
            ],
        });
        if (isAsa) {
            escrowTx.appForeignAssets = [config.assetId!];
        }

        const escrowAddr = this.algosdk.getApplicationAddress(config.appId);
        const paymentTx = !!config.assetId
            ? this.algosdk.makeAssetTransferTxnWithSuggestedParamsFromObject({
                suggestedParams,
                from: config.account,
                amount: config.amount,
                to: escrowAddr,
                assetIndex: config.assetId,
            })
            : this.algosdk.makePaymentTxnWithSuggestedParamsFromObject({
                suggestedParams,
                from: config.account,
                amount: config.amount,
                to: escrowAddr,
            });

        return [escrowTx, paymentTx];
    }

    unlock(suggestedParams: SuggestedParams, config: UnlockConfig) {
        const tx = this.algosdk.makeApplicationNoOpTxnFromObject({
            suggestedParams: {
                ...suggestedParams,
                flatFee: true,
                fee: Math.max(suggestedParams.fee, 1e3) * 2, //Addtn'l fee so the escrow can send a txn
            },
            from: config.account,
            appIndex: Number(config.appId), // TODO: why no bigint?,
            appArgs: [
                textToUint8('exit'),
                typeof config.lockId == 'string' ? textToUint8(config.lockId) : config.lockId,
            ]
        });

        if (config.assetId) {
            tx.appForeignAssets = [config.assetId];
        }

        return tx;
    }

    extend(suggestedParams: SuggestedParams, config: ExtendConfig) {
        const tx = this.algosdk.makeApplicationNoOpTxnFromObject({
            suggestedParams,
            from: config.account,
            appIndex: Number(config.appId), // TODO: why no bigint?,
            appArgs: [
                textToUint8('extend'),
                typeof config.lockId == 'string' ? textToUint8(config.lockId) : config.lockId,
                this.algosdk.encodeUint64(Math.floor(config.unlockDateMs / 1e3)),
            ],
        });

        return tx;
    }
}

type EscrowTxn = Transaction;
type PaymentTxn = Transaction;
type ExtraPaymentTxn = Transaction;

export interface LockConfig {
    account: string;
    amount: number;
    appId: bigint;
    lockId: string | Uint8Array;
    unlockDateMs: number;
    assetId?: number;
}

export interface UnlockConfig {
    account: string;
    appId: bigint;
    lockId: string | Uint8Array;
    assetId?: number;
}

export type ExtendConfig = Omit<LockConfig, 'assetId' | 'amount'>;
